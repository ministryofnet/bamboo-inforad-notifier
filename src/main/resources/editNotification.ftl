[@ww.textfield labelKey="radiator.api.token" name="apiToken" value="${apiToken!}" required='true'/]
[@ww.textfield labelKey="radiator.room" name="room" value="${room!}" required='true'/]
[@ww.checkbox labelKey="radiator.notify" name="notifyUsers" value="${notifyUsers!?string}"/]
