package net.gejza.bamboo.plugins.inforad.notifier;

import com.atlassian.bamboo.builder.BuildState;
import com.atlassian.bamboo.builder.LifeCycleState;
import com.atlassian.bamboo.deployments.results.DeploymentResult;
import com.atlassian.bamboo.notification.Notification;
import com.atlassian.bamboo.notification.NotificationTransport;
import com.atlassian.bamboo.plan.cache.ImmutablePlan;
import com.atlassian.bamboo.resultsummary.ResultsSummary;
import com.atlassian.bamboo.utils.HttpUtils;
import com.atlassian.bamboo.variable.CustomVariableContext;
import com.atlassian.event.Event;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.URIException;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
/**
 * Created by Milan on 18. 10. 2014.
 */
public class RadiatorNotificationTransport implements NotificationTransport {
    private static final Logger log = Logger.getLogger(RadiatorNotificationTransport.class);
    public static final String HIPCHAT_API_URL = "https://api.hipchat.com/v1/rooms/message?auth_token=";

    //colors, available options (as of 18.06.2013) are: yellow, red, green, purple and grey
    public static final String COLOR_UNKNOWN_STATE = "gray";
    public static final String COLOR_FAILED = "red";
    public static final String COLOR_SUCCESSFUL = "green";
    public static final String COLOR_IN_PROGRESS = "yellow";

    private final String apiToken;
    private final String room;
    private final String from = "Radiator";
    private final boolean notify;

    private final HttpClient client;

    @Nullable
    private final ImmutablePlan plan;
    @Nullable
    private final ResultsSummary resultsSummary;
    @Nullable
    private final DeploymentResult deploymentResult;
    private final Event event;

    public RadiatorNotificationTransport(String apiToken,
                                        String room,
                                        boolean notify,
                                        @Nullable ImmutablePlan plan,
                                        @Nullable ResultsSummary resultsSummary,
                                        @Nullable DeploymentResult deploymentResult,
                                        Event event,
                                        CustomVariableContext customVariableContext)
    {
        this.apiToken = customVariableContext.substituteString(apiToken);
        this.room = customVariableContext.substituteString(room);
        this.notify = notify;
        this.event = event;
        this.plan = plan;
        this.resultsSummary = resultsSummary;
        this.deploymentResult = deploymentResult;
        client = new HttpClient();

        try
        {
            URI uri = new URI(HIPCHAT_API_URL);
            setProxy(client, uri.getScheme());
        }
        catch (URIException e)
        {
            log.error("Unable to set up proxy settings, invalid URI encountered: " + e);
        }
        catch (URISyntaxException e)
        {
            log.error("Unable to set up proxy settings, invalid URI encountered: " + e);
        }
    }

    @Override
    public void sendNotification(Notification notification)
    {

        String message = (notification instanceof Notification.HtmlImContentProvidingNotification)
                ? ((Notification.HtmlImContentProvidingNotification) notification).getHtmlImContent()
                : notification.getIMContent();

        if (!StringUtils.isEmpty(message))
        {
            PostMethod method = setupPostMethod();
            method.setParameter("message",message);
            if (resultsSummary != null)
            {
                setMessageColor(method, resultsSummary);
            }
            else if (deploymentResult != null)
            {
                setMessageColor(method, deploymentResult);
            }
            else
            {
                setMessageColor(method, COLOR_UNKNOWN_STATE); //todo: might need to use different color in some cases
            }

            try
            {
                client.executeMethod(method);
            }
            catch (IOException e)
            {
                log.error("Error using Radiator API: " + e.getMessage(), e);
            }
        }
    }

    private void setMessageColor(PostMethod method, ResultsSummary result)
    {
        String color = COLOR_UNKNOWN_STATE;

        if (result.getBuildState() == BuildState.FAILED)
        {
            color = COLOR_FAILED;
        }
        else if (result.getBuildState() == BuildState.SUCCESS)
        {
            color = COLOR_SUCCESSFUL;
        }
        else if (LifeCycleState.isActive(result.getLifeCycleState()))
        {
            color = COLOR_IN_PROGRESS;
        }

        setMessageColor(method, color);
    }

    private void setMessageColor(PostMethod method, DeploymentResult deploymentResult)
    {
        String color = COLOR_UNKNOWN_STATE;

        if (deploymentResult.getDeploymentState() == BuildState.FAILED)
        {
            color = COLOR_FAILED;
        }
        else if (deploymentResult.getDeploymentState() == BuildState.SUCCESS)
        {
            color = COLOR_SUCCESSFUL;
        }
        else if (LifeCycleState.isActive(deploymentResult.getLifeCycleState()))
        {
            color = COLOR_IN_PROGRESS;
        }

        setMessageColor(method, color);
    }

    private void setMessageColor(PostMethod method, String colour)
    {
        method.addParameter("color", colour);
    }

    private PostMethod setupPostMethod()
    {
        PostMethod m = new PostMethod(HIPCHAT_API_URL + apiToken);
        m.addParameter("room_id", room);
        m.addParameter("from", from);
        m.addParameter("notify", (notify ? "1" : "0"));
        return m;
    }

    private static void setProxy(@NotNull final HttpClient client, @Nullable final String scheme) throws URIException
    {
        HttpUtils.EndpointSpec proxyForScheme = HttpUtils.getProxyForScheme(scheme);
        if (proxyForScheme!=null)
        {
            client.getHostConfiguration().setProxy(proxyForScheme.host, proxyForScheme.port);
        }
    }
}
