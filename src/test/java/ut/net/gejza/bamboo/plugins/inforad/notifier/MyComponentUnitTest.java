package ut.net.gejza.bamboo.plugins.inforad.notifier;

import org.junit.Test;
import net.gejza.bamboo.plugins.inforad.notifier.MyPluginComponent;
import net.gejza.bamboo.plugins.inforad.notifier.MyPluginComponentImpl;

import static org.junit.Assert.assertEquals;

public class MyComponentUnitTest
{
    @Test
    public void testMyName()
    {
        MyPluginComponent component = new MyPluginComponentImpl(null);
        assertEquals("names do not match!", "myComponent",component.getName());
    }
}